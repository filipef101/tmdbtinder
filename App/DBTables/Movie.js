const modelName = 'Movie'

class Model {
  static schema = {
    name: 'Movie',
    primaryKey: 'id',
    properties: {
      id: 'int',
      title: 'string',
      vote_average: 'double',
      original_title: 'string',
      vote_count: 'int',
      poster_path: 'string',
      release_date: 'string',
      like: 'string',
      overview: 'string'
    }
  }
}
function save (db, objectData, replace = true) {
  return db.create(modelName, objectData, replace)
}
function getLikes (db) {
  console.log("getlikes")
  console.log( db.objects(modelName))
  let allSwipes = db.objects(modelName).filtered('like==$0', "yes")
  // let results = { }
  // for (let k in allSwipes) {
  //   results[allSwipes[k].like] = 'yes'
  // }
  console.log('swepes')
  console.log(allSwipes)

  return allSwipes
}
function remove (db, id) {
  let results = db.objects(modelName).filtered('id==[c] $0', id)
  if (results.length === 1) {
    db.delete(results)
  }
}
module.exports = {
  Model,
  save,
  remove,
  getLikes
}
