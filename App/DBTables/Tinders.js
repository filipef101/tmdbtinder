const modelName = 'Tinders'

class Model {
  static schema = {
    name: modelName,
    properties: {
      likes: {type: 'list', objectType: 'Movie'},
      dislikes: {type: 'list', objectType: 'Movie'}

    }
  }
}

function save (db, objectData, replace = false) {
  return db.create(modelName, objectData, replace)
}

function remove (db, id) {
  let results = db.objects(modelName).filtered('id==[c] $0', id)
  if (results.length === 1) {
    db.delete(results)
  }
}

module.exports = {
  Model,
  save,
  remove
}
