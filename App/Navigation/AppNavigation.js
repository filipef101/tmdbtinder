import { StackNavigator } from 'react-navigation'
import TinderViewScreen from '../Containers/TinderViewScreen'
import DetailsView from '../Containers/DetailsView'
import MovieSearch from '../Containers/MovieSearch'
import MovieList from '../Containers/MovieList'
import MovieListFill from '../Containers/MovieListFill'

import LaunchScreen from '../Containers/LaunchScreen'
import hoistNonReactStatic from 'hoist-non-react-statics'
import React from 'react'

import styles from './Styles/NavigationStyles'

// https://github.com/react-community/react-navigation/issues/935
// https://github.com/vonovak/react-navigation-props-mapper
const mapNavigationStateParamsToProps = (WrappedComponent) => {
  const TargetComponent = props => {
    const { navigation: { state: { params } }, screenProps } = props
    return <WrappedComponent {...props} {...params} {...screenProps} />
  }
  WrappedComponent.displayName = `withMappedNavigationProps(${WrappedComponent.displayName ||
   WrappedComponent.name})`
  return hoistNonReactStatic(TargetComponent, WrappedComponent)
}
// Manifest of possible screens
const PrimaryNav = StackNavigator({
  TinderViewScreen: { screen: TinderViewScreen },
  DetailsView: { screen: mapNavigationStateParamsToProps(DetailsView) },
  MovieSearch: { screen: MovieSearch },
  MovieList: { screen: MovieList },
  MovieListFill: { screen: MovieListFill },
  LaunchScreen: { screen: LaunchScreen }
}, {
  // Default config for all screens
  headerMode: 'float',
  initialRouteName: 'MovieList',
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default PrimaryNav
