const Realm = require('realm')
// import AuthActions from '../Redux/AuthRedux'
// Import the models
const tables = [

  require('../DBTables/Movie')
  // require('../DBTables/MapPoint')

]
var realm

function getDbSingleton () {
  if (realm) return realm
  let models = []
  for (let k in Object.keys(tables)) {
    let table = tables[k]
      // setupSyncModelIfApplied(table)
    models.push(table.Model)
  }
  realm = new Realm({
    schema: models,
    schemaVersion: 1
  })
  realm.writeFix = (callback) => {
    realm.write(callback)
    realm.write(() => {})
  }
  realm.models = {} // We add the models object to the realm so that we can export it and easly import tables in other modules.
  for (let k in tables) {
    let table = tables[k]
    let tableName = table.Model.schema.name
    realm.models[tableName] = {}
    // For each table we'll add the functions that belong to that table filling in the first parameter which must always be the realm object
    // This is so that we don't have to be passing the realm object manually everytime we use a table function
    for (let objName in table) {
      let obj = table[objName]
      if (objName === 'Model' || objName === 'remoteObjectStructure' || objName === 'remoteUpdateObjects' || objName === 'remoteFilterToApply' || typeof obj !== 'function') {
        continue
      }
      realm.models[tableName][objName] = (...args) => {
        try {
          args.unshift(realm) // Prepend realm argument to list of function arguments
          return obj.apply(table, args)
        } catch (error) { // Realm crashed with cryptic errors, this may help to find the location of the error
          console.error(error)
        }
      }
    }
  }
  return realm
}

module.exports = getDbSingleton()
