import { StyleSheet } from 'react-native'
import { Colors } from '../../Themes/'

export default StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 0,
    backgroundColor: Colors.background,
    justifyContent: 'center',
    alignItems: 'center'
  },
  heading: {
    backgroundColor: '#F8F8F8'
  },
  headerRight: {
    marginRight:30
  },
  headerLeft: {
    margin:30
  },
  separator: {
    height: 1,
    backgroundColor: '#DDDDDD'
  },
  image: {

    width: 340,
    height: 500
  },
  content: {
    justifyContent: 'center',
    alignItems: 'center'

  },
  votes: {
    fontSize: 20,
    fontWeight: 'bold',
    margin: 5,
    color: '#48BBEC'
  },
  title: {
    fontSize: 25,
    margin: 5,
    fontWeight: 'bold',
    color: '#545454'
  },
  description: {
    fontSize: 18,
    margin: 5,
    color: '#656565'
  },
  release_date: {
    fontSize: 20,
    margin: 5,
    color: '#DC143C'
  }
})
