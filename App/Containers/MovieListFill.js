import React, { Component } from 'react'
import { View, Text, ListView, Image, TouchableHighlight, Keyboard } from 'react-native'
import { connect } from 'react-redux'
import SearchBar from 'react-native-searchbar'
import RoundedButton from '../Components/RoundedButton'
import MovieActions from '../Redux/MoviesRedux'

import PropTypes from 'prop-types'
import realm from '../Services/DB'

// Styles
import styles from './Styles/MovieListStyle'


class MovieListFill extends Component {
  // static navigationOptions = ({ navigation }) => ({
  //   header: null
  // })
  state: {
    dataSource: Object
  }
  static propTypes = {
    dispatch: PropTypes.func,
    getMoviesPopular: PropTypes.func,
    getMovieSearch: PropTypes.func
  }

  constructor (props) {
    super(props)

    this.props.getMoviesPopular()

    const dataObjects = [

    ]

    const rowHasChanged = (r1, r2) => r1 !== r2

    const ds = new ListView.DataSource({rowHasChanged})

    this.state = {
      dataSource: ds.cloneWithRows(dataObjects)
    }
  }

  _rowPressed= (rowData) => {
// .filter(prop => prop.guid === propertyGuid)[0]
    this.props.navigation.navigate('DetailsView', {movie: rowData})
  }

  renderRow =(rowData) => {
    return (
      <TouchableHighlight
        underlayColor= '#dddddd'
        onPress={() => this._rowPressed(rowData)} >

      <View style={styles.row}>

          <Image source={{ uri: 'https://image.tmdb.org/t/p/w320/' + rowData.poster_path }} style={styles.image_poster} />
          <Text style={styles.boldLabel}>{rowData.title}</Text>
          <Text style={styles.label}>{rowData.vote_average}</Text>

      </View>
      </TouchableHighlight>
    )
  }

  componentWillMount () {

  }
  componentWillReceiveProps (newProps) {
    // this.forceUpdate()
    if (newProps.movies.error === null && newProps.movies.data !== null) {
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(realm.models.Movie.getLikes())

      })
    } else {
      console.log('>> error')
    }
  }

  noRowData () {
    return this.state.dataSource.getRowCount() === 0
  }

  renderFooter = () => {
    return (
      <Text> - Footer - </Text>
    )
  }

  _handleResults=(results) => {
    this.props.getMovieSearch(this.searchBar.getValue())
  }
  tinderButtonA = () => {
    Keyboard.dismiss()
    this.props.navigation.navigate('TinderViewScreen')
  }

  render () {
    return (
      <View style={styles.container}>
        {/* <RoundedButton
          text=' '
        />
        <RoundedButton
          text='Tinder!'
          onPress={ () => this.tinderButtonA() }
        /> */}
      {/* <SearchBar hideBack handleResults={this._handleResults} ref={(ref) => this.searchBar = ref} showOnLoad /> */}
      <ListView
        contentContainerStyle={styles.listContent}
        dataSource={this.state.dataSource}

        renderRow={this.renderRow}
        pageSize={15}
      />
    </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    movies: state.movies
    // ...redux state to props here
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    getMoviesPopular: () => dispatch(MovieActions.moviesRequest()),
    getMovieSearch: (search) => dispatch(MovieActions.searchRequest(search))

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MovieListFill)
