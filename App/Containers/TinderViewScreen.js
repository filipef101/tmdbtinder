import React, { Component } from 'react'
import { ScrollView, Text, KeyboardAvoidingView, View, ListView, Image, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'
import SwipeCards from 'react-native-swipe-cards'
import MovieActions from '../Redux/MoviesRedux'
import PropTypes from 'prop-types'
import Icon from 'react-native-vector-icons/MaterialIcons'
import Iconz from 'react-native-vector-icons/Ionicons'
// Styles
import styles from './Styles/TinderViewScreenStyle'
import RoundedButton from '../Components/RoundedButton'
import realm from '../Services/DB'
class TinderViewScreen extends Component {

  static propTypes = {
    dispatch: PropTypes.func,
    getMoviesPopular: PropTypes.func

  }
  constructor (props) {
    super(props)

    // Datasource is always in state
    this.state = {
      Cards: cards
    }
  }
  Card (x) {
    return (
     <View style={styles.card}>
       <Image source ={{ uri: 'https://image.tmdb.org/t/p/w320/' + x.poster_path }} resizeMode="contain" style ={{width: 350, height: 350}} />
       <View style={{width: 350, height: 70, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
       <View style={{flexDirection: 'row', margin: 15, marginTop: 25}} >
       <Text style={{fontSize: 20, fontWeight: '300', color: '#444'}}>{x.title}, </Text>
       <Text style={{fontSize: 21, fontWeight: '200', color: '#444'}}>{x.vote_average}</Text>
       </View>
       <View style={{flexDirection: 'row'}}>
       {/* <View style={{padding: 13, borderLeftWidth: 1, borderColor: '#e3e3e3', alignItems: 'center', justifyContent: 'space-between'}}><Icon name='people-outline' size={20} color="#777" style={{}} /><Text style={{fontSize: 16, fontWeight: '200', color: '#555'}}>{x.friends}</Text></View> */}

       </View>

       </View>
       <View style={{padding: 13, borderLeftWidth: 1, borderColor: '#e3e3e3', alignItems: 'center', justifyContent: 'space-between'}}><Icon name='import-contacts' size={20} color="#777" /><Text style={{fontSize: 16, fontWeight: '200', color: '#555'}}>{x.overview}</Text></View>

     </View>
   )
  }
  noMore=()=>{
    return (
      <View style={styles.card} >
        <Text>No More Cards</Text>
      </View>
    )
  }
  componentWillMount () {
    this.props.getMoviesPopular()
  }
  componentWillReceiveProps (newProps) {
    console.log('teste')
    // this.forceUpdate()
    if (newProps.movies.error === null && newProps.movies.data !== null) {
      this.setState({
        Cards: newProps.movies.data.results
      })
    } else {
      console.log('>> error')
    }
  }
  yup= (card) => {
    // console.log(this.refs['swiper'])
    // realm.writeFix(() => {
    //   realm.models.Movie.save({
    //     id: card.id,
    //     title: card.title,
    //     vote_average: card.vote_average,
    //     original_title: card.original_title,
    //     vote_count: card.vote_count,
    //     poster_path: card.poster_path,
    //     release_date: card.release_date,
    //     overview: card.overview,
    //     like: 'yes'
    //   })
    // })

    this.refs['swiper']._forceRightSwipe()
  }
  nope () {
    console.log(this.refs['swiper'])
    this.refs['swiper']._goToNextCard()
  }
// k
  handleYup= (card) => {
    //this.refs['swiper']._goToNextCard()
    console.log(`Yup for ${card}`)
    console.log(realm.models)

    realm.writeFix(() => {
      realm.models.Movie.save({
        id: card.id,
        title: card.title,
        vote_average: card.vote_average,
        original_title: card.original_title,
        vote_count: card.vote_count,
        poster_path: card.poster_path,
        release_date: card.release_date,
        overview: card.overview,
        like: 'yes'
      })
    })
  }
  handleNope= (card) => {
  //  this.refs['swiper']._goToNextCard()
  realm.writeFix(() => {
    realm.models.Movie.save({
      id: card.id,
      title: card.title,
      vote_average: card.vote_average,
      original_title: card.original_title,
      vote_count: card.vote_count,
      poster_path: card.poster_path,
      release_date: card.release_date,
      overview: card.overview,
      like: 'no'
    })
  })
  }
  handleInfo= (card) => {
  //  this.refs['swiper']._goToNextCard()
    console.log(`info for ${card.text}`)
  }

  ButtonA = () => {
    // Keyboard.dismiss()
    this.props.navigation.navigate('MovieListFill')
  }

  render () {
    return (
      <View style={styles.container}>
     <SwipeCards
       ref = {'swiper'}
       cards={this.state.Cards}
       containerStyle = {{backgroundColor: '#f7f7f7', alignItems: 'center', margin: 20}}
       renderCard={(cardData) => this.Card(cardData)}
       renderNoMoreCards={this.noMore}
       handleYup={this.handleYup}
       handleNope={this.handleNope} />
       <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
       <TouchableOpacity style = {styles.buttons} onPress = {() => this.nope()}>
       <Iconz name='ios-close' size={45} color="#888" style={{}} />
       </TouchableOpacity>
       <TouchableOpacity style = {styles.buttonSmall}onPress = {() => this.info()}>
       <Iconz name='ios-information' size={25} color="#888" style={{}} />
       </TouchableOpacity>
       <TouchableOpacity style = {styles.buttons} onPress = {() => this.yup()}>
       <Iconz name='ios-heart-outline' size={36} color="#888" style={{marginTop: 5}} />
       </TouchableOpacity>
       </View>
       <RoundedButton
         text='See Matches!'
         onPress={ () => this.ButtonA() }
       />
       </View>
    )
  }
}
const cards = [

]

const mapStateToProps = (state) => {
  return {
    movies: state.movies
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getMoviesPopular: () => dispatch(MovieActions.moviesRequest())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TinderViewScreen)
