import React, { Component } from 'react'
import { ScrollView, Text, View, Image, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux'
import { Colors } from '../Themes/'
import Icon from 'react-native-vector-icons/Ionicons'

// Add Actions - replace 'Your' with whatever your reducer is called :)
// import YourActions from '../Redux/YourRedux'

// Styles
import styles from './Styles/DetailsViewStyle'

class DetailsView extends Component {
  // constructor (props) {
  //   super(props)
  //   this.state = {}
  // }

  static navigationOptions = ({ navigation }) => ({
    title: 'Movies',

    headerLeft:
      <TouchableOpacity style={styles.headerLeft} onPress={() => { navigation.goBack() }} >
        <Icon name='ios-arrow-back' size={25} color={Colors.charcoal}/>
       </TouchableOpacity>,
    headerRight:
     <TouchableOpacity style={styles.headerRight} onPress={() => { navigation.goBack() }} >
         <Icon name='md-close' size={25} color={Colors.charcoal}/>
     </TouchableOpacity>
  })

  render () {
    var movie = this.props.movie
    return (

      <ScrollView contentContainerStyle={styles.contentContainer}>
       <View style={styles.container}>
         <Image style={styles.image}
           source={{ uri: 'https://image.tmdb.org/t/p/w500/' + movie.poster_path }} />
         <View style={styles.heading}>
           <Text style={styles.title}>{movie.title}</Text>
           <View style={styles.separator}/>
         </View>
         <Text style={styles.release_date}>Release {movie.release_date}</Text>
         <Text style={styles.release_date}>Rating {movie.vote_average}</Text>
         <Text style={styles.description}>{movie.overview}</Text>
       </View>
     </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailsView)
