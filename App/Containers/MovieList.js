import React, { Component } from 'react'
import { View, Text, ListView, Image, TouchableHighlight, Keyboard } from 'react-native'
import { connect } from 'react-redux'
import SearchBar from 'react-native-searchbar'
import RoundedButton from '../Components/RoundedButton'
import MovieActions from '../Redux/MoviesRedux'

import PropTypes from 'prop-types'
// Styles
import styles from './Styles/MovieListStyle'

class MovieList extends Component {
  static navigationOptions = ({ navigation }) => ({
    header: null
  })
  state: {
    dataSource: Object
  }
  static propTypes = {
    dispatch: PropTypes.func,
    getMoviesPopular: PropTypes.func,
    getMovieSearch: PropTypes.func
  }

  constructor (props) {
    super(props)

    this.props.getMoviesPopular()

    /* ***********************************************************
    * STEP 1
    * This is an array of objects with the properties you desire
    * Usually this should come from Redux mapStateToProps
    *************************************************************/
    const dataObjects = [

    ]

    /* ***********************************************************
    * STEP 2
    * Teach datasource how to detect if rows are different
    * Make this function fast!  Perhaps something like:
    *   (r1, r2) => r1.id !== r2.id}
    *************************************************************/
    const rowHasChanged = (r1, r2) => r1 !== r2

    // DataSource configured
    const ds = new ListView.DataSource({rowHasChanged})

    // Datasource is always in state
    this.state = {
      dataSource: ds.cloneWithRows(dataObjects)
    }
  }

  /* ***********************************************************
  * STEP 3
  * `renderRow` function -How each cell/row should be rendered
  * It's our best practice to place a single component here:
  *
  * e.g.
    return <MyCustomCell title={rowData.title} description={rowData.description} />
  *************************************************************/

  _rowPressed= (rowData) => {

// .filter(prop => prop.guid === propertyGuid)[0]
    this.props.navigation.navigate('DetailsView', {movie: rowData})
  }

  renderRow =(rowData) => {
    return (
      <TouchableHighlight
        underlayColor= '#dddddd'
        onPress={() => this._rowPressed(rowData)} >

      <View style={styles.row}>

          <Image source={{ uri: 'https://image.tmdb.org/t/p/w320/' + rowData.poster_path }} style={styles.image_poster} />
          <Text style={styles.boldLabel}>{rowData.title}</Text>
          <Text style={styles.label}>{rowData.vote_average}</Text>

      </View>
      </TouchableHighlight>
    )
  }

  /* ***********************************************************
  * STEP 4
  * If your datasource is driven by Redux, you'll need to
  * reset it when new data arrives.
  * DO NOT! place `cloneWithRows` inside of render, since render
  * is called very often, and should remain fast!  Just replace
  * state's datasource on newProps.
  *
  * e.g.
    componentWillReceiveProps (newProps) {
      if (newProps.someData) {
        this.setState(prevState => ({
          dataSource: prevState.dataSource.cloneWithRows(newProps.someData)
        }))
      }
      this.props.getMoviesPopular()
    }
  *************************************************************/
  componentWillMount () {
    this.props.getMoviesPopular()
  }
  componentWillReceiveProps (newProps) {
    // this.forceUpdate()
    if (newProps.movies.error === null && newProps.movies.data !== null) {
      this.setState({
        dataSource: this.state.dataSource.cloneWithRows(newProps.movies.data.results)

      })
    } else {
      console.log('>> error')
    }

  }
  // Used for friendly AlertMessage
  // returns true if the dataSource is empty
  noRowData () {
    return this.state.dataSource.getRowCount() === 0
  }

  // Render a footer.
  renderFooter = () => {
    return (
      <Text> - Footer - </Text>
    )
  }
  _handleResults=(results) => {

    this.props.getMovieSearch(this.searchBar.getValue())
  }
  tinderButtonA = () => {
    Keyboard.dismiss()
    this.props.navigation.navigate('TinderViewScreen')
  }

  render () {
    return (
      <View style={styles.container}>
        <RoundedButton
          text=' '
        />
        <RoundedButton
          text='Tinder!'
          onPress={ () => this.tinderButtonA() }
        />
      <SearchBar hideBack handleResults={this._handleResults} ref={(ref) => this.searchBar = ref} showOnLoad />
      <ListView
        contentContainerStyle={styles.listContent}
        dataSource={this.state.dataSource}

        renderRow={this.renderRow}
        pageSize={15}
      />
    </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    movies: state.movies
    // ...redux state to props here
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    getMoviesPopular: () => dispatch(MovieActions.moviesRequest()),
    getMovieSearch: (search) => dispatch(MovieActions.searchRequest(search))

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MovieList)
